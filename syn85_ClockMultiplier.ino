//	Clock Multiplier

#include <avr/io.h>
#include <avr/interrupt.h>
#include <EEPROM.h>

#define clockOutPin	3		//	PB3
#define clockInPin	1		//	PB1
#define ratchetPin		0		//	PB0
#define multiplyPin		A1
#define LEDPin			4		//	PB4

long inputPeriod;
long endPulseTime;
long pulseDuration = 5000;		//	microseconds
long endPeriod;
long period;
long now;
long newClockIn;
long lastInputPeriod;
long multPeriod = 5000;

long startTimer = 0;
long endTimer;

int range;
int mapped;

boolean disableLED = false;
boolean clockIn;
boolean pulsing;
boolean pulseFinished = true;
boolean checkedClock;
boolean oldClock;
boolean clock;
boolean newClock;

int counter;
boolean flash;
boolean flashing;
unsigned long endCycle;
boolean echo = true;
boolean CCO;					//	Not used. Later?

void setup()
{
	pinMode(clockInPin, INPUT);
	pinMode(clockOutPin, OUTPUT);
	pinMode(ratchetPin, INPUT);
	pinMode(LEDPin, OUTPUT);

	checkSetup();

	echo = EEPROM.read(0);
	//echo = false;
	/*int temp = EEPROM.read(0);
	if (temp == 0) {
		echo = false;
	}
	else { echo = true; }*/

	//echo = true;

	PCMSK |= (1 << PCINT1);		// pin change mask: listen to portb bit 1 PB1
	GIMSK |= (1 << PCIE);		// enable PCINT interrupt
	sei();						// enable all interrupts

	lastInputPeriod = micros();
	multPeriod = lastInputPeriod / 2;

	//	Blink LED on startup.

	digitalWrite(LEDPin, HIGH);
	delay(100);
	digitalWrite(LEDPin, LOW);
	delay(100);

	newClock = false;
}

void loop()
{
	if (newClock)											//	Restart on a new clock pulse
	{
		now = micros();
		startTimer = now;
		inputPeriod = startTimer - endTimer;
		endTimer = now;
		endPulseTime = now + pulseDuration;
		endPeriod = now + multPeriod;
		endCycle = now + (inputPeriod);
		if (pulsing)
		{
			endPulse();										//	End existing clock if high
		}
		if (echo)
		{
			if (echo || endCycle)	//	Send pulse on new clock if echo is on or a full cycle has ended
			{
				startPulse();
			}
		}
		if (!echo)
		{
			if (checkRatchetPin())
			{
				startPulse();
			}
		}

		//startLED();
		analogWrite(LEDPin, 200);
		flashing = true;
		pulseFinished = false;
		newClock = false;
		counter = 1;
	}

	now = micros();

	if ((now >= endPulseTime) && (pulseFinished == false))	//	Falling edge of generated clock
	{
		pulseFinished = true;
		endPulse();
		endLED();

		//	BEGIN SLOW CALCULATION SAFE ZONE

		range = analogRead(multiplyPin);
		mapped = map(range, 0, 1023, 1, 24);
		multPeriod = inputPeriod / mapped;

		//	END SLOW CALCULATION SAFE ZONE
	}

	//
	//	End of a period. Rising edge of generated clock.
	//

	if ((now >= endPeriod) && pulseFinished && (startTimer != 0))
	{
		now = micros();			//	Start a new clock and keep going until a new input clock.
		if (now < endCycle)
		{
			if (checkRatchetPin())
			{
				startPulse();
			}
			endPulseTime = now + pulseDuration;
			endPeriod = now + multPeriod;
			pulseFinished = false;
			startLED();
			counter++;
		}
		else {
			pulseFinished = true;
			endPulse();
			endLED();
			flash = false;
			startTimer = 0;
		}
	}
}

/***********************************************************************************************************

				FAST IO

*/

void startPulse() {
	PORTB |= (1 << clockOutPin);							// clockOutputPin high
	pulsing = true;
}

void endPulse() {
	PORTB &= ~(1 << clockOutPin);							// clockOutputPin low
	pulsing = false;
}

void startLED() {
	if (!disableLED) {
		//PORTB |= (1 << LEDPin);							// LEDPin high
		analogWrite(LEDPin, 50);
	}
}

void endLED() {
	if (!disableLED) {
		analogWrite(LEDPin, 0);
		//PORTB &= ~(1 << LEDPin);							// LEDPin low
	}
}

boolean checkRatchetPin()
{
	return (bitRead(PINB, ratchetPin));
}

ISR(PCINT0_vect)											//	Generate an interrupt on a clock input

{
	newClock = bitRead(PINB, clockInPin);
}

//	Configuration
//	1. Echo original clock
//	2. Continuous output?

/**********************************************************************************************************

				Boot-time Configuration:

-Turn off module
-Patch from input to output (first jack to third jack).
-Turn pot to desired programming position (full left, full right).
	-Full left: Echo ON (default)
	-Full right: Echo OFF

-Turn power on.
-Module LED will flash for 1 second to show it's been programmed.
-Remove patch cable.
-Module should function as desired, but may need to be power-cycled again (without patch cable).

location 0 - Echo							- 0 = OFF  1 = ON
//x Location 1 - Continuous Clock Output CCO	- 0 = OFF  1 = ON

Echo: When no multiplication (knob full left at zero) echo the incoming clock to the output. Default is to echo.

**********************************************************************************************************/

void checkSetup()
{
	bool fail = false;

	//	Start secret knock to check if setup patch cables enabled

	for (int i = 1; i <= 10; i++)
	{
		digitalWrite(clockOutPin, HIGH);
		delay(random(10) + 1);								//	Not random, but generates a sequence
		if (digitalRead(clockInPin) != HIGH) { fail = true; }
		digitalWrite(clockOutPin, LOW);
		delay(random(10) + 1);
		if (digitalRead(clockInPin) != LOW) { fail = true; }
	}

	//	If output is connected to input, read the pot and store the value

	if (!fail)
	{
		//	Set timings
		int pot = analogRead(multiplyPin);
		if (pot <= 300) {
			EEPROM.write(0, 1);								//	Location 0, ECHO on
		}
		else if (pot >= 700) {
			EEPROM.write(0, 0);								//	Location 0, ECHO off
		}

		digitalWrite(LEDPin, HIGH);
		delay(1000);
		digitalWrite(LEDPin, LOW);
		delay(500);
	}

	//	If EEPROM is empty (ie. new firmware uploaded).
	//int temp = EEPROM.read(0);
	//if (temp >=2) {
	//	EEPROM.write(0, 1);		//	Default ECHO on
	//}
}